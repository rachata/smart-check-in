import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-check-in',
  templateUrl: './check-in.component.html',
  styleUrls: ['./check-in.component.sass']
})
export class CheckInComponent implements OnInit {


  pass  = "assets/images/tick.png";
  close  = "assets/images/close.png";

  wait = "assets/images/bell.png";

  classImgClose= "cd-movie";
  classImgPass= "cd-picture";
  classImgWait= "cd-location";


  fullname = "";
  date = "";
  address = "";
  cid = "";

  statusCard = "";
  statusColor = ""

  statusImage = ""
  statusImageColor = ""
  
  stepImg1;
  stepClass1;

  stepImg2;
  stepClass2;

  stepImg3;
  stepClass3;

  stepImg4;
  stepClass4;

  fileImage = "";

  qrcode = "";

  @ViewChild('qrcode') private elementRef: ElementRef;
  

  public ngAfterViewInit(): void {
    // this.elementRef.nativeElement.focus();
  }
  
  constructor(private http : HttpClient) {
      this.stepImg1 = this.close;
      this.stepClass1 = this.classImgClose;

      this.stepImg2 = this.close;
      this.stepClass2 = this.classImgClose;

      this.stepImg3 = this.close;
      this.stepClass3 = this.classImgClose;

      this.stepImg4 = this.close;
      this.stepClass4 = this.classImgClose;


      this.fileImage = "assets/images/profile.png";

   }

  ngOnInit(): void {
  }


  onClickStep1(){
    this.stepImg1 =this.pass;
    this.stepClass1 = this.classImgPass;

    this.stepImg2 =this.wait;
    this.stepClass2 = this.classImgWait;

    this.statusCard ="กำลังดึงข้อมูล รอสักครู่....";
    this.statusColor="color: red;"

   
    // this.callCard();


    this.address = '36/7 ม.3 ต.เขารูปช้าง อ.เมือง จ.สงขลา 90000';
    this.cid = '1900101177674';
    this.date = '29/10/2540';
    this.fullname = 'นายรัชฏ สุรียะ';


    this.statusColor="color: green;"
          this.statusCard ="ดึงข้อมูลสำเร็จ โปรดตรวจสอบข้อมูล";

    let el = document.getElementById("step2");
    el.scrollIntoView();


  }

  onClickStep2(){
    
    this.stepImg2 =this.pass;
    this.stepClass2 = this.classImgPass;

    this.stepImg3 =this.wait;
    this.stepClass3 = this.classImgWait;
    let el = document.getElementById("step3");
    el.scrollIntoView();
  }

  onClickStep3(){
    
    this.stepImg3 =this.pass;
    this.stepClass3 = this.classImgPass;

    this.stepImg4 =this.wait;
    this.stepClass4 = this.classImgWait;

    this.statusImageColor="color: red;"
    this.statusImage = "กำลังถ่ายภาพ...";
    this.fileImage = "assets/images/cam.png";

    this.statusImageColor="color: green;"
    this.statusImage = "ถ่ายภาพสำเร็จ";

    this.fileImage = "http://192.168.1.129/image_smart_check_in/1603285719535.jpg";
    
    // http://192.168.1.129/image_smart_check_in/1603285719535.jpg
    // this.callImage();
  }

  onClickStep4(){
    
  

    this.stepImg4 =this.pass;
    this.stepClass4 = this.classImgPass;

    this.qrcode = "กำลังอ่าน"

    let el = document.getElementById("step5");
    el.scrollIntoView();
  }

  callCard(){


    this.http.get("http://192.168.1.129:5000/card", {})

    .subscribe(

      data => {


        if(data){
          this.statusColor="color: green;"
          this.statusCard ="ดึงข้อมูลสำเร็จ โปรดตรวจสอบข้อมูล";
        }
        this.address = data['address'];
        this.cid = data['cid'];
        this.date = data['dateOfBirth'];
        this.fullname = data['thName'];


        console.log(JSON.stringify(data))
      },

      error => {


        console.log("error")
      }

    );

  }


  callImage(){


    this.http.get("http://192.168.1.129:5000/image", {})

    .subscribe(

      data => {

        if(data){
          this.statusImageColor="color: green;"
          this.statusImage = "ถ่ายภาพสำเร็จ";


    let el = document.getElementById("step4");
    el.scrollIntoView();

    document.getElementById("qrcodeText").focus();


          this.fileImage = "http://192.168.1.171/image_smart_check_in/"+data['file'];
        }
        console.log(JSON.stringify(data))
      },

      error => {


        console.log("error")
      }

    );

  }

}
