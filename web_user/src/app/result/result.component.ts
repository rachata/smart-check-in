import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.sass']
  
  
})


export class ResultComponent implements OnInit {



  pass  = "assets/images/tick.png";
  close  = "assets/images/close.png";

  wait = "assets/images/bell.png";

  classImgClose= "cd-movie";
  classImgPass= "cd-picture";
  classImgWait= "cd-location";


  fullname = "";
  date = "";
  address = "";
  cid = "";



  statusCard = "";
  statusColor = ""

  stepImg1;
  stepClass1;

  stepImg2;
  stepClass2;

  stepImg3;
  stepClass3;

  stepImg4;
  stepClass4;


  stepImg5;
  stepClass5;


  stepImg6;
  stepClass6;


  statusImage = ""
  statusImageColor = ""

  stepImg7;
  stepClass7;


  fileImage   

  constructor(private http : HttpClient) { 
    this.stepImg1 = this.wait;
    this.stepClass1 = this.classImgWait;


    this.stepImg2 = this.close;
    this.stepClass2 = this.classImgClose;

    this.stepImg3 = this.close;
    this.stepClass3 = this.classImgClose;

    this.stepImg4 = this.close;
    this.stepClass4 = this.classImgClose;


    this.stepImg5 = this.close;
    this.stepClass5 = this.classImgClose;

    this.stepImg6 = this.close;
    this.stepClass6 = this.classImgClose;


    this.stepImg7 = this.close;
    this.stepClass7 = this.classImgClose;


    this.statusImageColor="color: red;"
    this.statusImage = "กำลังถ่ายภาพ...";

    this.fileImage = "assets/images/cam.png";

  }

  ngOnInit(): void {
  }


  onClickStep1(){
    this.stepImg1 = this.pass;
    this.stepClass1 = this.classImgPass;


    this.stepImg2 = this.wait;
    this.stepClass2 = this.classImgWait;

    this.statusCard = "กำลังอ่านข้อมูลบัตร..."
    this.statusColor = "color : red;"


    let el = document.getElementById("step1");
    el.scrollIntoView();


    this.statusColor="color: green;"
          this.statusCard ="ดึงข้อมูลสำเร็จ โปรดตรวจสอบข้อมูล";

          this.address = '36/7 ม.3 ต.เขารูปช้าง อ.เมือง จ.สงขลา 90000';
    this.cid = '1900101177674';
    this.date = '29/10/2540';
    this.fullname = 'นายรัชฏ สุรียะ';
    // this.callCard();

  }


  callCard(){


    this.http.get("http://192.168.1.171:5000/card", {})

    .subscribe(

      data => {


        if(data){
          this.statusColor="color: green;"
          this.statusCard ="ดึงข้อมูลสำเร็จ โปรดตรวจสอบข้อมูล";
        }
        this.address = data['address'];
        this.cid = data['cid'];
        this.date = data['dateOfBirth'];
        this.fullname = data['thName'];


        console.log(JSON.stringify(data))
      },

      error => {


        console.log("error")
      }

    );

  }




  onClickStep2(){
    this.stepImg2 = this.pass;
    this.stepClass2 = this.classImgPass;


    this.stepImg3 = this.wait;
    this.stepClass3 = this.classImgWait;

    let el = document.getElementById("step2");
    el.scrollIntoView();

    document.getElementById("qrcodeText").focus();

  }


  onClickStep3(){

    this.stepImg3 = this.pass;
    this.stepClass3 = this.classImgPass;


    this.stepImg4 = this.wait;
    this.stepClass4 = this.classImgWait;
    
    let el = document.getElementById("step3");
    el.scrollIntoView();
  }


  onClickStep4(){

    this.stepImg4 = this.pass;
    this.stepClass4 = this.classImgPass;


    this.stepImg5 = this.wait;
    this.stepClass5 = this.classImgWait;

    let el = document.getElementById("step4");
    el.scrollIntoView();

  }

  onClickStep5(){
    let el = document.getElementById("step5");
    el.scrollIntoView();
  }

  onClickStep6(){
    let el = document.getElementById("step6");
    el.scrollIntoView();


    this.statusImageColor="color: green;"
          this.statusImage = "ถ่ายภาพสำเร็จ";

          
          this.fileImage = "http://192.168.1.129/image_smart_check_in/1603285950219.jpg";

    // this.callImage();
  }

  callImage(){


    this.http.get("http://192.168.1.129:5000/image", {})

    .subscribe(

      data => {

        if(data){
          this.statusImageColor="color: green;"
          this.statusImage = "ถ่ายภาพสำเร็จ";

          
          this.fileImage = "http://192.168.1.129/image_smart_check_in/1603285950219";
        
          // this.fileImage = "http://192.168.1.129/image_smart_check_in/"+data['file'];
        
        }
        console.log(JSON.stringify(data))
      },

      error => {


        console.log("error")
      }

    );

  }


}
