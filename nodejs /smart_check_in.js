var app = require('express')();
var mysql = require('mysql');
var crypto = require('crypto');
var request = require('request')
var dateFormat = require('dateformat');
var bodyParser = require('body-parser');
var multer = require('multer');

var port = process.env.PORT || 8787;
var pathImg = '';

var idEvent = 0;
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

var Storage = multer.diskStorage({
    destination: function (req, file, callback) {
        callback(null, "/var/www/html/image_checkin");
        // callback(null, "image");
    },
    filename: function (req, file, callback) {



        pathImg = Date.now() + "_" + file.originalname;


        callback(null, pathImg);
    }
});

var upload = multer({
    storage: Storage
}).array("imgUploader", 3);



var con = mysql.createConnection({
    host: "34.87.185.176",
    user: "smart",
    password: "@#SmartCheckIn2020",
    database: "smart_check_in"
});

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "*");
    res.header("Access-Control-Allow-Headers", "*");
    next();
});


con.connect(function (err) {
    if (err) throw err;
});



app.post("/checkin", function (req, res) {
    upload(req, res, function (err) {
        if (err) {

            return res.json({ "status": 500, message: err })
        }

        var fName = req.body.fName;
        var lNmae = req.body.lName
        var cid = req.body.cid
        var fullName = req.body.fullName
        var address = req.body.address
        var birthday = req.body.birthday
        var qrcode = req.body.qrcode

        var query = "select * from prisoner where cid = '" + cid + "'";

        con.query(query, function (err, result, fields) {
            if (err) {
                res.json({ "status": 500, message: err.stack })
                return;
            }
            var key = Object.keys(result);


            if (key.length == 1) {



                var id = result[0]['id']
                var b = dateFormat(new Date(), "yyyy/mm/dd HH:MM:ss");

                query = "UPDATE prisoner set last_result = '" + b + "' WHERE cid = '" + cid + "'";

                con.query(query, function (err, result, fields) {
                    if (err) {
                        res.json({ "status": 500, message: err.stack })
                        return;
                    }
                    query = "insert into check_in_prisoner (id_p ,qrcode ,img) values(" + id + " , '" + qrcode + "' , '" + pathImg + "')"
                    con.query(query, function (err, result, fields) {
                        if (err) {
                            res.json({ "status": 500, message: err.stack })
                            return;
                        }

                        res.json({ "status": 200 })
                    })



                });

            } else {

                var b = dateFormat(new Date(), "yyyy/mm/dd HH:MM:ss");
                query = "INSERT INTO prisoner (cid , last_result) values ('" + cid + "' , '" + b + "')";

                con.query(query, function (err, result, fields) {
                    if (err) {
                        res.json({ "status": 500, message: err.stack })
                        return;
                    }

                    var id = result.insertId;

                    query = "INSERT INTO detail_prisoner (id_p , fullname , firstname , lastname  , address , birthday) values (" + id + " , '" + fullName + "' , '" + fName + "' , '" + lNmae + "' , '" + address + "' , '" + birthday + "')";


                    con.query(query, function (err, result, fields) {
                        if (err) {
                            res.json({ "status": 500, message: err.stack })
                            return;
                        }




                        query = "insert into check_in_prisoner (id_p ,qrcode ,img) values(" + id + " , '" + qrcode + "' , '" + pathImg + "')"
                        con.query(query, function (err, result, fields) {
                            if (err) {
                                res.json({ "status": 500, message: err.stack })
                                return;
                            }

                            res.json({ "status": 200 })
                        })


                    });

                });


            }

        })

    })

})

app.get("/prisoner", function (req, res) {

    var query = "select prisoner.id , cid , fullname , firstname , lastname , address ,birthday , dt , last_result  from prisoner inner join detail_prisoner on prisoner.id = detail_prisoner.id_p";

    con.query(query, function (err, result, fields) {
        if (err) {
            res.json({ "status": 500, message: err.stack })
            return;
        }



        for (var i = 0; i < Object.keys(result).length; i++) {

            var b = dateFormat(new Date(result[i]['birthday']), "dd/mm/yyyy");
            var dt = dateFormat(new Date(result[i]['dt']), "dd/mm/yyyy HH:MM:ss");
            var l = dateFormat(new Date(result[i]['last_result']), "dd/mm/yyyy HH:MM:ss");




            var dayLast = new Date(dateFormat(new Date(result[i]['last_result']), "yyyy/mm/dd HH:MM:ss"))
            var day = new Date(dateFormat(new Date(), "yyyy/mm/dd HH:MM:ss"))




            const diffTime = Math.abs(day - dayLast);
            var diffDays = Math.floor(diffTime / 86400000); // days
            var diffHrs = Math.floor((diffTime % 86400000) / 3600000); // hours
            var diffMins = Math.round(((diffTime % 86400000) % 3600000) / 60000);


            var strDiff = "";

            if (diffDays != 0) {
                strDiff += diffDays + " วัน "
            }

            if (diffHrs != 0) {

                strDiff += diffHrs + " ชั่วโมง "
            }

            if (diffMins != 0) {

                strDiff += diffMins + " นาที"
            }



            result[i]['dt'] = dt
            result[i]['birthday'] = b
            result[i]['last_result'] = l
            result[i]['diff'] = strDiff

        }

        res.json({ "status": 200, message: result })
    })

})



app.get("/prisoner/:id", function (req, res) {

    var id = req.params.id;
    var query = "select prisoner.id , prisoner.cid ,  detail_prisoner.fullname , detail_prisoner.firstname , detail_prisoner.lastname , detail_prisoner.address , detail_prisoner.birthday , detail_prisoner.img from prisoner inner join detail_prisoner on id_p = prisoner.id WHERE prisoner.id  = " + id;

    con.query(query, function (err, result, fields) {
        if (err) {
            res.json({ "status": 500, message: err.stack })
            return;
        }

        res.json({ "status": 200, message: result[0] })

    })

})



app.get("/result/:id", function (req, res) {


    var id = req.params.id
    var query = "select check_in_prisoner.id_p , check_in_prisoner.qrcode  as 'cqr' , check_in_prisoner.img as 'ckimg' , check_in_prisoner.dt as 'ckdt' , result_prisoner.qrcode as 'rqr' , result_question , result_strip ,  img_strip , result_prisoner.dt as 'rdt'  from check_in_prisoner inner join result_prisoner on check_in_prisoner.qrcode  = result_prisoner.qrcode where check_in_prisoner.id_p = " + id;


    var resultP = 0;
    var resultN = 0;
    con.query(query, function (err, result, fields) {
        if (err) {
            res.json({ "status": 500, message: err.stack })
            return;
        }



        for (var i = 0; i < Object.keys(result).length; i++) {

            var ckdt = dateFormat(new Date(result[i]['ckdt']), "dd/mm/yyyy HH:MM:ss");
            var rdt = dateFormat(new Date(result[i]['rdt']), "dd/mm/yyyy HH:MM:ss");


            var dayLast = new Date(dateFormat(new Date(result[i]['ckdt']), "yyyy/mm/dd HH:MM:ss"))
            var day = new Date(dateFormat(new Date(result[i]['rdt']), "yyyy/mm/dd HH:MM:ss"))



            if (result[i]['result_strip'] == 1) {
                resultP = resultP + 1
            }


            if (result[i]['result_strip'] == 0) {
                resultN = resultN + 1
            }

            const diffTime = Math.abs(day - dayLast);
            var diffDays = Math.floor(diffTime / 86400000); // days
            var diffHrs = Math.floor((diffTime % 86400000) / 3600000); // hours
            var diffMins = Math.round(((diffTime % 86400000) % 3600000) / 60000);


            var strDiff = "";

            if (diffDays != 0) {
                strDiff += diffDays + " วัน "
            }

            if (diffHrs != 0) {

                strDiff += diffHrs + " ชั่วโมง "
            }

            if (diffMins != 0) {

                strDiff += diffMins + " นาที"
            }

            result[i]['rdt'] = rdt
            result[i]['ckdt'] = ckdt
            result[i]['diff'] = strDiff

        }

        res.json({ "status": 200, message: result, count: Object.keys(result).length, resultP: resultP, resultN, resultN })
    })

})


app.get("/result/detail/:id", function (req, res) {

    var qrcode = req.params.id
    var query = "select check_in_prisoner.id_p , check_in_prisoner.qrcode  as 'cqr' , check_in_prisoner.img as 'ckimg' , check_in_prisoner.dt as 'ckdt' , result_prisoner.qrcode as 'rqr' , result_question , result_strip ,  img_strip , result_prisoner.dt as 'rdt'  from check_in_prisoner inner join result_prisoner on check_in_prisoner.qrcode  = result_prisoner.qrcode where check_in_prisoner.qrcode = '" + qrcode + "'";


    var resultP = 0;
    var resultN = 0;
    con.query(query, function (err, result, fields) {
        if (err) {
            res.json({ "status": 500, message: err.stack })
            return;
        }



        for (var i = 0; i < Object.keys(result).length; i++) {

            var ckdt = dateFormat(new Date(result[i]['ckdt']), "dd/mm/yyyy HH:MM:ss");
            var rdt = dateFormat(new Date(result[i]['rdt']), "dd/mm/yyyy HH:MM:ss");


            var dayLast = new Date(dateFormat(new Date(result[i]['ckdt']), "yyyy/mm/dd HH:MM:ss"))
            var day = new Date(dateFormat(new Date(result[i]['rdt']), "yyyy/mm/dd HH:MM:ss"))



            if (result[i]['result_strip'] == 1) {
                resultP = resultP + 1
            }


            if (result[i]['result_strip'] == 0) {
                resultN = resultN + 1
            }

            const diffTime = Math.abs(day - dayLast);
            var diffDays = Math.floor(diffTime / 86400000); // days
            var diffHrs = Math.floor((diffTime % 86400000) / 3600000); // hours
            var diffMins = Math.round(((diffTime % 86400000) % 3600000) / 60000);


            var strDiff = "";

            if (diffDays != 0) {
                strDiff += diffDays + " วัน "
            }

            if (diffHrs != 0) {

                strDiff += diffHrs + " ชั่วโมง "
            }

            if (diffMins != 0) {

                strDiff += diffMins + " นาที"
            }

            result[i]['rdt'] = rdt
            result[i]['ckdt'] = ckdt
            result[i]['diff'] = strDiff

        }

        res.json({ "status": 200, message: result, count: Object.keys(result).length, resultP: resultP, resultN, resultN })
    })



})


app.post("/result", function (req, res) {
    upload(req, res, function (err) {

        if (err) {

            return res.json({ "status": 500, message: err })
        }


        var cid = req.body.cid
        var qrcode = req.body.qrcode
        var resultStrip = req.body.resultStrip
        var resultQuestion = req.body.resultQuestion

        var query = "select * from prisoner where cid = '" + cid + "'";

        con.query(query, function (err, result, fields) {
            if (err) {
                res.json({ "status": 500, message: err.stack })
                return;
            }
            var key = Object.keys(result);


            if (key.length == 1) {

                var id = result[0]['id'];
                query = "Select * from check_in_prisoner where id_p =  " + id + " AND qrcode = '" + qrcode + "'";


                con.query(query, function (err, result, fields) {
                    if (err) {
                        res.json({ "status": 500, message: err.stack })
                        return;
                    }
                    var key = Object.keys(result);


                    if (key.length >= 1) {

                        query = "insert into result_prisoner (id_p , qrcode , result_strip , result_question ,img_strip) values (" + id + " , '" + qrcode + "' , " + resultStrip + " , '" + resultQuestion + "' , '" + pathImg + "')"

                        con.query(query, function (err, result, fields) {
                            if (err) {
                                res.json({ "status": 500, message: err.stack })
                                return;
                            }

                            res.json({ "status": 200 })

                        })
                    } else {
                        return res.json({ "status": 204, message: "การตรวจสอบ QR Code ผิดพลาด" })
                    }

                })

            } else {
                return res.json({ "status": 204, message: "ยังไม่มีการรายงานตัว กรุณารายงานตัวก่อน" })
            }


        })

    })

})



// update qrcode set status = 1 where qrcode = '""'


// select * from qrcode where status = 0;


app.post("/qrcode/success", function (req, res) {


    var qrcodes = req.body.qrcode;

    var key  = Object.keys(qrcodes);
    for(var i = 0 ; i < key.length; i++){

        var query = "update qrcode set status = 1 where qrcode =  '"+qrcodes[i]['qr']+"'";

        con.query(query, function (err, result, fields) {
            if (err) {
                res.json({ "status": 500, message: err.stack })
                return;
            }
        })
    }

    res.json({ "status": 200 })
})




app.get("/qrcode/all", function (req, res) {

    var query = "select * from qrcode";

    con.query(query, function (err, result, fields) {
        if (err) {
            res.json({ "status": 500, message: err.stack })
            return;
        }
        res.json({ "status": 200, message: result })

    })

})

app.get("/qrcode/print", function (req, res) {

    var query = "select * from qrcode where status = 0";

    con.query(query, function (err, result, fields) {
        if (err) {
            res.json({ "status": 500, message: err.stack })
            return;
        }
        res.json({ "status": 200, message: result })

    })

})


app.get("/qrcode", function (req, res) {

    var query = "SELECT UUID() as 'qr';";

    con.query(query, function (err, result, fields) {
        if (err) {
            res.json({ "status": 500, message: err.stack })
            return;
        }


        var qrcode = result[0]['qr']
        var query = "insert into qrcode (qrcode) values ('" + qrcode + "')";

        con.query(query, function (err, result, fields) {
            if (err) {
                res.json({ "status": 500, message: err.stack })
                return;
            }

            res.json({ "status": 200, message: qrcode })
        })

    })
})
app.listen(port, function () {
    console.log('Starting node.js on port ' + port);
});
