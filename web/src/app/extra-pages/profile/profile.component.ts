import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgxQrcodeElementTypes, NgxQrcodeErrorCorrectionLevels } from '@techiediaries/ngx-qrcode';
import pdfMake from "pdfmake/build/pdfmake";
import pdfFonts from "pdfmake/build/vfs_fonts";
import { Observable, Observer } from 'rxjs';

pdfMake.vfs = pdfFonts.pdfMake.vfs;

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  idUser
  idResult
  name;
  address;
  idn
  imgPath;


  imgc;
  imgr;
  question;
  result;
  dtc
  dtr

  imgc64;

  elementType = NgxQrcodeElementTypes.URL;
  correctionLevel = NgxQrcodeErrorCorrectionLevels.HIGH;
  value


  constructor(private http: HttpClient, public route: ActivatedRoute) {

    this.route.params.subscribe(params => {
      console.log("params" + JSON.stringify(params))
      this.idUser = Number(params['idUser'])
      this.idResult = params['id']

      this.callAPI()


      
      // this.getBase64ImageFromURL("http://34.87.185.176/Image_Survey/1595867491830_burned.png").subscribe(base64data => {    
      //   console.log(base64data);
      //   // this is the image as dataUrl
      //   'data:image/jpg;base64,' + base64data;
      // });

      

    })
  }
  ngOnInit() { }

  async callAPI() {


    this.http.get('http://34.87.185.176:8787/prisoner/' + this.idUser, {})
      .subscribe(

        datas => {

          if (datas['status'] == 200) {


            this.name = datas['message']['fullname']
            this.address = datas['message']['address']
            this.idn = datas['message']['cid']
            this.imgPath = datas['message']['img']

            console.log(JSON.stringify(datas))
          }
        })



    this.http.get('http://34.87.185.176:8787/result/detail/' + this.idResult, {})
      .subscribe(

        datas => {

          console.log("wefwefwef");
          console.log(JSON.stringify(datas))
          if (datas['status'] == 200) {

            if (datas['message'][0]['result_strip'] == 1) {
              datas['message'][0]['result_strip'] = "2 ขีด"
            } else {
              datas['message'][0]['result_strip'] = "1 ขีด"
            }
            this.imgc = datas['message'][0]['ckimg']
            this.imgr = datas['message'][0]['img_strip']
            this.question = datas['message'][0]['result_question']
            this.result = datas['message'][0]['result_strip']
            this.dtc = datas['message'][0]['ckdt']
            this.dtr = datas['message'][0]['rdt'] + " " + datas['message'][0]['diff']
            this.value = datas['message'][0]['cqr']
          }
        })

  }




  getBase64ImageFromURL(url: string) {
    return Observable.create((observer: Observer<string>) => {
      // create an image object
      let img = new Image();
      img.crossOrigin = 'Anonymous';
      img.src = url;
      if (!img.complete) {
          // This will call another method that will create image from url
          img.onload = () => {
          observer.next(this.getBase64Image(img));
          observer.complete();
        };
        img.onerror = (err) => {
           observer.error(err);
        };
      } else {
          observer.next(this.getBase64Image(img));
          observer.complete();
      }
    });
 }
 
  getBase64Image(img: HTMLImageElement) {
    // We create a HTML canvas object that will create a 2d image
    var canvas = document.createElement("canvas");
    canvas.width = img.width;
    canvas.height = img.height;
    var ctx = canvas.getContext("2d");
    // This will draw image    
    ctx.drawImage(img, 0, 0);
    // Convert the drawn image to Data URL
    var dataURL = canvas.toDataURL("image/png");
    return dataURL.replace(/^data:image\/(png|jpg);base64,/, "");
  }



  async export() {



    pdfMake.fonts = {
      THSarabunNew: {
        normal: "THSarabunNew.ttf",
        bold: 'THSarabunNew Bold.ttf',
      },
      Roboto: {
        normal: 'Roboto-Regular.ttf',
      }
    };


    // const documentDefinition = {
    //   content: [

    //     { text: "รายงานสรุปผลการคัดกรอง", fontSize: 20, alignment: 'center', bold: true },

    //     { text: "ข้อมูลส่วนบุคคล", fontSize: 16, alignment: 'left', bold: true },
    //     { text: "ชื่อ-นามสกุล : " + this.name, fontSize: 16, alignment: 'left', bold: false, margin: [10, 0, 0, 0] },
    //     { text: "ที่อยู่ : " + this.address, fontSize: 16, alignment: 'left', bold: false, margin: [10, 0, 0, 0] },
    //     { text: "เลขประจำตัวประชาชน : " + this.idn, fontSize: 16, alignment: 'left', bold: false, margin: [10, 0, 0, 0] },


    //     { text: "รายงานตัว", fontSize: 16, alignment: 'left', bold: true, margin: [0, 10, 0, 0] },
    //     { text: "วันเวลา : " + this.dtc, fontSize: 16, alignment: 'left', bold: false, margin: [10, 0, 0, 0] },
    //     { text: "QRCode : ", fontSize: 16, alignment: 'left', bold: false, margin: [10, 0, 0, 0] },
    //     { qr: this.value, margin: [10, 0, 0, 0] },
    //     { text: "รูปถ่ายจากกล้องตอนรายงานตัว : ", fontSize: 16, alignment: 'left', bold: false, margin: [10, 5, 0, 0] },



    //     { text: "ส่งผล", fontSize: 16, alignment: 'left', bold: true, margin: [0, 10, 0, 0] },
    //     { text: "วันเวลา : " + this.dtr, fontSize: 16, alignment: 'left', bold: false, margin: [10, 0, 0, 0] },
    //     { text: "ผล : " + this.result, fontSize: 16, color: (this.result == "2 ขีด") ? "red" : "green", alignment: 'left', bold: true, margin: [10, 0, 0, 0] },
    //     { text: "คำถาม ใช้สารเสพติดหรือไม่ : " + this.question, fontSize: 16, alignment: 'left', bold: false, margin: [10, 0, 0, 0] },
    //     { text: "รูปถ่าย Strip : ", fontSize: 16, alignment: 'left', bold: false, margin: [10, 5, 0, 0] },

    //   ], defaultStyle: {
    //     font: 'THSarabunNew'
    //   }
    // };


    const documentDefinition = {
      pageMargins: [5, 5, 5, 5],
      content: [
       

        {
          margin: [25, 0, 0, 0],
          layout: 'lightHorizontalLines', // optional
          table: {

            headerRows: 1,
            widths: [ 'auto', 'auto', 'auto', 'auto' , 'auto'  , 'auto'],
    
            body: [
              [{qr: "weodeopwmdpowemdopmewdopmwepodmweopdmopwemdopwemopdmweopdmopwedmopwemdopwoemdop" , fit: '100'} ,
              {qr: "weodeopwmdpowemdopmewdopmwepodmweopdmopwemdopwemopdmweopdmopwedmopwemdopwoemdop" , fit: '100'} ,
              {qr: "weodeopwmdpowemdopmewdopmwepodmweopdmopwemdopwemopdmweopdmopwedmopwemdopwoemdop" , fit: '100'} ,
              {qr: "weodeopwmdpowemdopmewdopmwepodmweopdmopwemdopwemopdmweopdmopwedmopwemdopwoemdop" , fit: '100'} ,
              {qr: "weodeopwmdpowemdopmewdopmwepodmweopdmopwemdopwemopdmweopdmopwedmopwemdopwoemdop" , fit: '100'},
              {qr: "weodeopwmdpowemdopmewdopmwepodmweopdmopwemdopwemopdmweopdmopwedmopwemdopwoemdop" , fit: '100'}   ],

              [{qr: "weodeopwmdpowemdopmewdopmwepodmweopdmopwemdopwemopdmweopdmopwedmopwemdopwoemdop" , fit: '100'} ,
              {qr: "weodeopwmdpowemdopmewdopmwepodmweopdmopwemdopwemopdmweopdmopwedmopwemdopwoemdop" , fit: '100'} ,
              {qr: "weodeopwmdpowemdopmewdopmwepodmweopdmopwemdopwemopdmweopdmopwedmopwemdopwoemdop" , fit: '100'} ,
              {qr: "weodeopwmdpowemdopmewdopmwepodmweopdmopwemdopwemopdmweopdmopwedmopwemdopwoemdop" , fit: '100'} ,
              {qr: "weodeopwmdpowemdopmewdopmwepodmweopdmopwemdopwemopdmweopdmopwedmopwemdopwoemdop" , fit: '100'},
              {qr: "weodeopwmdpowemdopmewdopmwepodmweopdmopwemdopwemopdmweopdmopwedmopwemdopwoemdop" , fit: '100'}   ],

              [{qr: "weodeopwmdpowemdopmewdopmwepodmweopdmopwemdopwemopdmweopdmopwedmopwemdopwoemdop" , fit: '100'} ,
              {qr: "weodeopwmdpowemdopmewdopmwepodmweopdmopwemdopwemopdmweopdmopwedmopwemdopwoemdop" , fit: '100'} ,
              {qr: "weodeopwmdpowemdopmewdopmwepodmweopdmopwemdopwemopdmweopdmopwedmopwemdopwoemdop" , fit: '100'} ,
              {qr: "weodeopwmdpowemdopmewdopmwepodmweopdmopwemdopwemopdmweopdmopwedmopwemdopwoemdop" , fit: '100'} ,
              {qr: "weodeopwmdpowemdopmewdopmwepodmweopdmopwemdopwemopdmweopdmopwedmopwemdopwoemdop" , fit: '100'},
              {qr: "weodeopwmdpowemdopmewdopmwepodmweopdmopwemdopwemopdmweopdmopwedmopwemdopwoemdop" , fit: '100'}   ],

              [{qr: "weodeopwmdpowemdopmewdopmwepodmweopdmopwemdopwemopdmweopdmopwedmopwemdopwoemdop" , fit: '100'} ,
              {qr: "weodeopwmdpowemdopmewdopmwepodmweopdmopwemdopwemopdmweopdmopwedmopwemdopwoemdop" , fit: '100'} ,
              {qr: "weodeopwmdpowemdopmewdopmwepodmweopdmopwemdopwemopdmweopdmopwedmopwemdopwoemdop" , fit: '100'} ,
              {qr: "weodeopwmdpowemdopmewdopmwepodmweopdmopwemdopwemopdmweopdmopwedmopwemdopwoemdop" , fit: '100'} ,
              {qr: "weodeopwmdpowemdopmewdopmwepodmweopdmopwemdopwemopdmweopdmopwedmopwemdopwoemdop" , fit: '100'},
              {qr: "weodeopwmdpowemdopmewdopmwepodmweopdmopwemdopwemopdmweopdmopwedmopwemdopwoemdop" , fit: '100'}   ],

              [{qr: "weodeopwmdpowemdopmewdopmwepodmweopdmopwemdopwemopdmweopdmopwedmopwemdopwoemdop" , fit: '100'} ,
              {qr: "weodeopwmdpowemdopmewdopmwepodmweopdmopwemdopwemopdmweopdmopwedmopwemdopwoemdop" , fit: '100'} ,
              {qr: "weodeopwmdpowemdopmewdopmwepodmweopdmopwemdopwemopdmweopdmopwedmopwemdopwoemdop" , fit: '100'} ,
              {qr: "weodeopwmdpowemdopmewdopmwepodmweopdmopwemdopwemopdmweopdmopwedmopwemdopwoemdop" , fit: '100'} ,
              {qr: "weodeopwmdpowemdopmewdopmwepodmweopdmopwemdopwemopdmweopdmopwedmopwemdopwoemdop" , fit: '100'},
              {qr: "weodeopwmdpowemdopmewdopmwepodmweopdmopwemdopwemopdmweopdmopwedmopwemdopwoemdop" , fit: '100'}   ],

              [{qr: "weodeopwmdpowemdopmewdopmwepodmweopdmopwemdopwemopdmweopdmopwedmopwemdopwoemdop" , fit: '100'} ,
              {qr: "weodeopwmdpowemdopmewdopmwepodmweopdmopwemdopwemopdmweopdmopwedmopwemdopwoemdop" , fit: '100'} ,
              {qr: "weodeopwmdpowemdopmewdopmwepodmweopdmopwemdopwemopdmweopdmopwedmopwemdopwoemdop" , fit: '100'} ,
              {qr: "weodeopwmdpowemdopmewdopmwepodmweopdmopwemdopwemopdmweopdmopwedmopwemdopwoemdop" , fit: '100'} ,
              {qr: "weodeopwmdpowemdopmewdopmwepodmweopdmopwemdopwemopdmweopdmopwedmopwemdopwoemdop" , fit: '100'},
              {qr: "weodeopwmdpowemdopmewdopmwepodmweopdmopwemdopwemopdmweopdmopwedmopwemdopwoemdop" , fit: '100'}   ],

              [{qr: "weodeopwmdpowemdopmewdopmwepodmweopdmopwemdopwemopdmweopdmopwedmopwemdopwoemdop" , fit: '100'} ,
              {qr: "weodeopwmdpowemdopmewdopmwepodmweopdmopwemdopwemopdmweopdmopwedmopwemdopwoemdop" , fit: '100'} ,
              {qr: "weodeopwmdpowemdopmewdopmwepodmweopdmopwemdopwemopdmweopdmopwedmopwemdopwoemdop" , fit: '100'} ,
              {qr: "weodeopwmdpowemdopmewdopmwepodmweopdmopwemdopwemopdmweopdmopwedmopwemdopwoemdop" , fit: '100'} ,
              {qr: "weodeopwmdpowemdopmewdopmwepodmweopdmopwemdopwemopdmweopdmopwedmopwemdopwoemdop" , fit: '100'},
              {qr: "weodeopwmdpowemdopmewdopmwepodmweopdmopwemdopwemopdmweopdmopwedmopwemdopwoemdop" , fit: '100'}   ],

              [{qr: "weodeopwmdpowemdopmewdopmwepodmweopdmopwemdopwemopdmweopdmopwedmopwemdopwoemdop" , fit: '100'} ,
              {qr: "weodeopwmdpowemdopmewdopmwepodmweopdmopwemdopwemopdmweopdmopwedmopwemdopwoemdop" , fit: '100'} ,
              {qr: "weodeopwmdpowemdopmewdopmwepodmweopdmopwemdopwemopdmweopdmopwedmopwemdopwoemdop" , fit: '100'} ,
              {qr: "weodeopwmdpowemdopmewdopmwepodmweopdmopwemdopwemopdmweopdmopwedmopwemdopwoemdop" , fit: '100'} ,
              {qr: "weodeopwmdpowemdopmewdopmwepodmweopdmopwemdopwemopdmweopdmopwedmopwemdopwoemdop" , fit: '100'},
              {qr: "weodeopwmdpowemdopmewdopmwepodmweopdmopwemdopwemopdmweopdmopwedmopwemdopwoemdop" , fit: '100'}   ],

              [{qr: "weodeopwmdpowemdopmewdopmwepodmweopdmopwemdopwemopdmweopdmopwedmopwemdopwoemdop" , fit: '100'} ,
              {qr: "weodeopwmdpowemdopmewdopmwepodmweopdmopwemdopwemopdmweopdmopwedmopwemdopwoemdop" , fit: '100'} ,
              {qr: "weodeopwmdpowemdopmewdopmwepodmweopdmopwemdopwemopdmweopdmopwedmopwemdopwoemdop" , fit: '100'} ,
              {qr: "weodeopwmdpowemdopmewdopmwepodmweopdmopwemdopwemopdmweopdmopwedmopwemdopwoemdop" , fit: '100'} ,
              {qr: "weodeopwmdpowemdopmewdopmwepodmweopdmopwemdopwemopdmweopdmopwedmopwemdopwoemdop" , fit: '100'},
              {qr: "weodeopwmdpowemdopmewdopmwepodmweopdmopwemdopwemopdmweopdmopwedmopwemdopwoemdop" , fit: '100'}   ],

              [{qr: "weodeopwmdpowemdopmewdopmwepodmweopdmopwemdopwemopdmweopdmopwedmopwemdopwoemdop" , fit: '100'} ,
              {qr: "weodeopwmdpowemdopmewdopmwepodmweopdmopwemdopwemopdmweopdmopwedmopwemdopwoemdop" , fit: '100'} ,
              {qr: "weodeopwmdpowemdopmewdopmwepodmweopdmopwemdopwemopdmweopdmopwedmopwemdopwoemdop" , fit: '100'} ,
              {qr: "weodeopwmdpowemdopmewdopmwepodmweopdmopwemdopwemopdmweopdmopwedmopwemdopwoemdop" , fit: '100'} ,
              {qr: "weodeopwmdpowemdopmewdopmwepodmweopdmopwemdopwemopdmweopdmopwedmopwemdopwoemdop" , fit: '100'},
              {qr: "weodeopwmdpowemdopmewdopmwepodmweopdmopwemdopwemopdmweopdmopwedmopwemdopwoemdop" , fit: '100'}   ],

              [{qr: "weodeopwmdpowemdopmewdopmwepodmweopdmopwemdopwemopdmweopdmopwedmopwemdopwoemdop" , fit: '100'} ,
              {qr: "weodeopwmdpowemdopmewdopmwepodmweopdmopwemdopwemopdmweopdmopwedmopwemdopwoemdop" , fit: '100'} ,
              {qr: "weodeopwmdpowemdopmewdopmwepodmweopdmopwemdopwemopdmweopdmopwedmopwemdopwoemdop" , fit: '100'} ,
              {qr: "weodeopwmdpowemdopmewdopmwepodmweopdmopwemdopwemopdmweopdmopwedmopwemdopwoemdop" , fit: '100'} ,
              {qr: "weodeopwmdpowemdopmewdopmwepodmweopdmopwemdopwemopdmweopdmopwedmopwemdopwoemdop" , fit: '100'},
              {qr: "weodeopwmdpowemdopmewdopmwepodmweopdmopwemdopwemopdmweopdmopwedmopwemdopwoemdop" , fit: '100'}   ],

              [{qr: "weodeopwmdpowemdopmewdopmwepodmweopdmopwemdopwemopdmweopdmopwedmopwemdopwoemdop" , fit: '100'} ,
              {qr: "weodeopwmdpowemdopmewdopmwepodmweopdmopwemdopwemopdmweopdmopwedmopwemdopwoemdop" , fit: '100'} ,
              {qr: "weodeopwmdpowemdopmewdopmwepodmweopdmopwemdopwemopdmweopdmopwedmopwemdopwoemdop" , fit: '100'} ,
              {qr: "weodeopwmdpowemdopmewdopmwepodmweopdmopwemdopwemopdmweopdmopwedmopwemdopwoemdop" , fit: '100'} ,
              {qr: "weodeopwmdpowemdopmewdopmwepodmweopdmopwemdopwemopdmweopdmopwedmopwemdopwoemdop" , fit: '100'},
              {qr: "weodeopwmdpowemdopmewdopmwepodmweopdmopwemdopwemopdmweopdmopwedmopwemdopwoemdop" , fit: '100'}   ],

              [{qr: "weodeopwmdpowemdopmewdopmwepodmweopdmopwemdopwemopdmweopdmopwedmopwemdopwoemdop" , fit: '100'} ,
              {qr: "weodeopwmdpowemdopmewdopmwepodmweopdmopwemdopwemopdmweopdmopwedmopwemdopwoemdop" , fit: '100'} ,
              {qr: "weodeopwmdpowemdopmewdopmwepodmweopdmopwemdopwemopdmweopdmopwedmopwemdopwoemdop" , fit: '100'} ,
              {qr: "weodeopwmdpowemdopmewdopmwepodmweopdmopwemdopwemopdmweopdmopwedmopwemdopwoemdop" , fit: '100'} ,
              {qr: "weodeopwmdpowemdopmewdopmwepodmweopdmopwemdopwemopdmweopdmopwedmopwemdopwoemdop" , fit: '100'},
              {qr: "weodeopwmdpowemdopmewdopmwepodmweopdmopwemdopwemopdmweopdmopwedmopwemdopwoemdop" , fit: '100'}   ],
          
            ]
          }
        }

        
        // {
        //   columns: [
        //     {
            
        //       margin: [10, 5, 10, 5],
        //       width: '10%',
        //       qr: this.value,
        //     },
        //     // {
            
        //     //   margin: [10, 5, 10, 5],
        //     //   width: '15%',
        //     //   qr: this.value
        //     // },
        //     // {
            
        //     //   margin: [10, 5, 10, 5],
        //     //   width: '20%',
        //     //   qr: this.value
        //     // },
        //     // {
            
        //     //   margin: [10, 5, 10, 5],
        //     //   width: '20%',
        //     //   qr: this.value
        //     // },
        //   ],
        //   // optional space between columns
        //   columnGap: 10
        // },

      ], defaultStyle: {
        font: 'THSarabunNew'
      }
    };

    pdfMake.createPdf(documentDefinition).download()();


  }

}
