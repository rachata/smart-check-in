import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import pdfMake from "pdfmake/build/pdfmake";
import pdfFonts from "pdfmake/build/vfs_fonts";
import {
  FormGroup,
  FormBuilder,
  FormControl,
  Validators
} from '@angular/forms';

import { selectRowInterface } from 'src/app/tables/ngx-datatable/ngx-datatable.component';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { NgxQrcodeElementTypes, NgxQrcodeErrorCorrectionLevels } from '@techiediaries/ngx-qrcode';

@Component({
  selector: 'app-qrcode',
  templateUrl: './qrcode.component.html',
  styleUrls: ['./qrcode.component.sass']
})
export class QrcodeComponent implements OnInit {


  rows = [];
  selectedRowData: selectRowInterface;
  newUserImg = 'assets/images/user/user1.jpg';
  data = [];
  filteredData = [];
  editForm: FormGroup;
  register: FormGroup;
  selectedOption: string;
  columns = [

    { prop: "qrcode-id" },
    { prop: "qrCode" },
    { prop: "print" },
  ];


  elementType = NgxQrcodeElementTypes.URL;
  correctionLevel = NgxQrcodeErrorCorrectionLevels.HIGH;
  @ViewChild(DatatableComponent, { static: false }) table: DatatableComponent;
  constructor(private http: HttpClient, public router: Router) {
  }
  ngOnInit(): void {


    this.callAPI()

  }

  print() {

    var qrcodeArr = [];

    this.http.get('http://34.87.185.176:8787/qrcode/print', {})
      .subscribe(

        datas => {



          console.log(JSON.stringify(datas))
          if (datas['status'] == 200) {

            var key = Object.keys(datas['message']);

            if (key.length >= 1) {

              var j = 0;
              var i = 0;
              var k = 0;


              while (true) {



                console.log(qrcodeArr.length);
                if (qrcodeArr.length == 0) {

                  var temp = [];
                  var raw = {};

                  raw['qr'] = datas['message'][k]['qrcode']
                  raw['fit'] = 100;

                  temp.push(raw);
                  qrcodeArr.push(temp)

                } else {
                  k = k + 1;
                  j = j + 1;

                  if (j == 5) {
                    j = 0;
                    i = i + 1;

                    var temp = [];
                    var raw = {};

                    raw['qr'] = datas['message'][k]['qrcode']
                    raw['fit'] = 100;

                    temp.push(raw);
                    qrcodeArr.push(temp)



                  } else {
                    var raw = {};

                    raw['qr'] = datas['message'][k]['qrcode']
                    raw['fit'] = 100;
                    qrcodeArr[i].push(raw);
                  }
                }

                if (k == key.length - 1) {


                  console.log("key.length ", key.length);
                  console.log("K", k)
                  if (j != 4) {

                    for (var z = j; z < 4; z++) {
                      var raw = {};

                      raw['text'] = ""

                      qrcodeArr[i].push(raw);

                    }

                  }
                  break;
                }
              }


              console.log(JSON.stringify(qrcodeArr))

              pdfMake.fonts = {
                THSarabunNew: {
                  normal: "THSarabunNew.ttf",
                  bold: 'THSarabunNew Bold.ttf',
                },
                Roboto: {
                  normal: 'Roboto-Regular.ttf',
                }
              };


              const documentDefinition = {
                pageMargins: [5, 5, 5, 5],
                content: [


                  {
                    margin: [25, 0, 0, 0],
                    layout: 'lightHorizontalLines', // optional
                    table: {
                      widths: ['auto', 'auto', 'auto', 'auto', 'auto'],

                      body: qrcodeArr
                    }
                  }

                ], defaultStyle: {
                  font: 'THSarabunNew'
                }
              };

              pdfMake.createPdf(documentDefinition).download()();


            }
          }

          console.log("QRCODE", JSON.stringify(qrcodeArr));

        })






  }



  callAPI() {


    this.http.get('http://34.87.185.176:8787/qrcode/all', {})
      .subscribe(

        datas => {

          this.data = [];
          this.filteredData = [];


          datas['message'].reverse()
          console.log(JSON.stringify(datas))
          for (var i = 0; i < Object.keys(datas['message']).length; i++) {
            var raw = {};
            raw['qrcode-id'] = datas['message'][i]['qrcode']
            raw['qrCode'] = datas['message'][i]['qrcode']

            if (datas['message'][i]['status'] == 0) {

              datas['message'][i]['status'] = "ยังไม่พิมพ์"
            } else {
              datas['message'][i]['status'] = "พิมพ์แล้ว"
            }
            raw['print'] = datas['message'][i]['status']


            this.data.push(raw);
            this.filteredData.push(raw);
          }



          console.log(JSON.stringify(datas))
        })


  }

  addRow() {



    this.http.get('http://34.87.185.176:8787/qrcode', {})
      .subscribe(

        datas => {


          console.log(JSON.stringify(datas))
          this.callAPI();

        })


  }
  onActivate(event) {

    console.log(JSON.stringify(event.type))
    if (event.type == 'click') {
      console.log(JSON.stringify(event.row));
      // this.router.navigateByUrl('/result/'+event.row.id);
    }
  }

  arrayRemove(array, id) {
    return array.filter(function (element) {
      return element.id != id;
    });
  }


  filterDatatable(event) {
    // get the value of the key pressed and make it lowercase
    const val = event.target.value.toLowerCase();
    // get the amount of columns in the table
    const colsAmt = this.columns.length;
    // get the key names of each column in the dataset

    console.log(JSON.stringify(this.filteredData));
    const keys = Object.keys(this.filteredData[0]);
    // assign filtered matches to the active datatable
    this.data = this.filteredData.filter(function (item) {
      // iterate through each row's column data
      for (let i = 0; i < colsAmt; i++) {
        // check for a match


        if (
          item[keys[i]]
            .toString()
            .toLowerCase()
            .indexOf(val) !== -1 ||
          !val
        ) {
          // found match, return true to add to result set
          return true;
        }
      }
    });
    // whenever the filter changes, always go back to the first page
    this.table.offset = 0;
  }
}
