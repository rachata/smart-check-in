import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import {
  FormGroup,
  FormBuilder,
  FormControl,
  Validators
} from '@angular/forms';

import { selectRowInterface } from 'src/app/tables/ngx-datatable/ngx-datatable.component';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
declare const $: any;
@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {

  rows = [];
  selectedRowData: selectRowInterface;
  newUserImg = 'assets/images/user/user1.jpg';
  data = [];
  filteredData = [];
  editForm: FormGroup;
  register: FormGroup;
  selectedOption: string;
  columns = [

    { prop: "fullName" },
    { prop: "idn" },
    { prop: "age" },
    { prop: "dtlast" },
  ];

  public areaChartOptions = {
    responsive: true,
    tooltips: {
      mode: 'index',
      titleFontSize: 12,
      titleFontColor: '#000',
      bodyFontColor: '#000',
      backgroundColor: '#fff',
      cornerRadius: 3,
      intersect: false
    },
    legend: {
      display: false,
      labels: {
        usePointStyle: true
      }
    },
    scales: {
      xAxes: [
        {
          display: true,
          gridLines: {
            display: false,
            drawBorder: false
          },
          scaleLabel: {
            display: false,
            labelString: 'Month'
          },
          ticks: {
            fontColor: '#9aa0ac' // Font Color
          }
        }
      ],
      yAxes: [
        {
          display: true,
          gridLines: {
            display: false,
            drawBorder: false
          },
          scaleLabel: {
            display: true,
            labelString: 'คน'
          },
          ticks: {
            fontColor: '#9aa0ac' // Font Color
          }
        }
      ]
    },
    title: {
      display: false,
      text: 'Normal Legend'
    }
  };
  areaChartData = [
    {
      label: 'New Patients',
      data: [0, 105, 190, 140, 270],
      borderWidth: 4,
      pointStyle: 'circle',
      pointRadius: 4,
      borderColor: 'rgba(37,188,232,.7)',
      pointBackgroundColor: 'rgba(37,188,232,.2)',
      backgroundColor: 'rgba(37,188,232,.2)',
      pointBorderColor: 'transparent'
    },
    {
      label: 'Old Patients',
      data: [0, 152, 80, 250, 190],
      borderWidth: 4,
      pointStyle: 'circle',
      pointRadius: 4,
      borderColor: 'rgba(72,239,72,.7)',
      pointBackgroundColor: 'rgba(72,239,72,.2)',
      backgroundColor: 'rgba(72,239,72,.2)',
      pointBorderColor: 'transparent'
    }
  ];
  areaChartLabels = ['January', 'February', 'March', 'April', 'May'];

  public barChartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true,
    scales: {
      xAxes: [
        {
          ticks: {
            fontFamily: 'Poppins',
            fontColor: '#9aa0ac' // Font Color
          }
        }
      ],
      yAxes: [
        {
          ticks: {
            beginAtZero: true,
            fontFamily: 'Poppins',
            fontColor: '#9aa0ac' // Font Color
          }
        }
      ]
    }
  };
  public barChartLabels: string[] = [
    '10/10/2563',
    '11/10/2563',
    '12/10/2563',
    '13/10/2563',
    '14/10/2563',
    '15/10/2563',
    '16/10/2563'
  ];
  public barChartType = 'bar';
  public barChartLegend = false;
  public barChartData: any[] = [
    { data: [58, 60, 74, 78, 55, 64, 42], label: 'ผลเป็นบวก' },
    { data: [30, 45, 51, 22, 79, 35, 82], label: 'ผลเป็นลบ' }
  ];
  public barChartColors: Array<any> = [
    {
      backgroundColor: 'rgba(211,211,211,1)',
      borderColor: 'rgba(211,211,211,1)',
      pointBackgroundColor: 'rgba(211,211,211,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(211,211,211,0.8)'
    },
    {
      backgroundColor: 'rgba(110, 104, 193, 1)',
      borderColor: 'rgba(110, 104, 193,1)',
      pointBackgroundColor: 'rgba(110, 104, 193,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(110, 104, 193,0.8)'
    }
  ];

  @ViewChild(DatatableComponent, { static: false }) table: DatatableComponent;
  constructor(private http: HttpClient , public router: Router) { 
  }

  callAPI(){


    var raw = {};
    raw['id'] = "1"
    raw['fullName'] = "นายรัชฏ สุรียะ"
    raw['age'] =  "22 ปี"
    raw['idn'] = "1900101177674"
    raw['dtlast'] = "21/10/2020 ; 15 นาทีที่แล้ว"


    console.log(JSON.stringify(raw))
    this.data.push(raw)

    this.filteredData.push(raw)
    
    this.http.get('http://34.87.185.176:8787/prisoner', {})
    .subscribe(

      datas => {

        if(datas['status'] == 200){
          var key = Object.keys(datas['message'])

          this.data = [];

          for(var i = 0; i < key.length;i++){
  
            // var raw = {};
            // raw['id'] = datas['message'][i]['id']
            // raw['fullName'] = datas['message'][i]['fullname']
            // raw['age'] = datas['message'][i]['birthday']
            // raw['idn'] = datas['message'][i]['cid']
            // raw['dtlast'] = datas['message'][i]['last_result'] + " ; " + datas['message'][i]['diff']


            var raw = {};
            raw['id'] = "1"
            raw['fullName'] = "นายรัชฏ สุรียะ"
            raw['age'] =  "22 ปี"
            raw['idn'] = "1900101177674"
            raw['dtlast'] = "21/10/2020 ; 15 นาทีที่แล้ว"
  
  
            console.log(JSON.stringify(raw))
            this.data.push(raw)

            this.filteredData.push(raw)
  
  
          }

          console.log(JSON.stringify(this.data))
        }

      }
    )
  }

  onActivate(event) {

    console.log(JSON.stringify(event.type ))
    if (event.type == 'click') {
      console.log(JSON.stringify(event.row));
      this.router.navigateByUrl('/result/'+event.row.id);
    }
  }



  ngOnInit() {

    this.callAPI();

  }

  arrayRemove(array, id) {
    return array.filter(function (element) {
      return element.id != id;
    });
  }


  filterDatatable(event) {
    // get the value of the key pressed and make it lowercase
    const val = event.target.value.toLowerCase();
    // get the amount of columns in the table
    const colsAmt = this.columns.length;
    // get the key names of each column in the dataset

    console.log(JSON.stringify(this.filteredData));
    const keys = Object.keys(this.filteredData[0]);
    // assign filtered matches to the active datatable
    this.data = this.filteredData.filter(function (item) {
      // iterate through each row's column data
      for (let i = 0; i < colsAmt; i++) {
        // check for a match


        if (
          item[keys[i]]
            .toString()
            .toLowerCase()
            .indexOf(val) !== -1 ||
          !val
        ) {
          // found match, return true to add to result set
          return true;
        }
      }
    });
    // whenever the filter changes, always go back to the first page
    this.table.offset = 0;
  }

}
