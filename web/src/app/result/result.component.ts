import { HttpClient } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { selectRowInterface } from '../tables/ngx-datatable/ngx-datatable.component';

@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.sass']
})
export class ResultComponent implements OnInit {


  count = 0;
  resultP = 0;
  resultN = 0;
  rows = [];
  selectedRowData: selectRowInterface;
  newUserImg = 'assets/images/user/user1.jpg';
  data = [];
  filteredData = [];
  editForm: FormGroup;
  register: FormGroup;
  selectedOption: string;
  columns = [

    { prop: "result" },
    { prop: "question" },
    { prop: "dtcheckin" },
    { prop: "dtresult" },
    { prop: "dtsmart" },
  ];


  idUser
  @ViewChild(DatatableComponent, { static: false }) table: DatatableComponent;
  constructor(private http: HttpClient , public route: ActivatedRoute, public router: Router) {

    this.route.params.subscribe(params => {
      console.log("params" + JSON.stringify(params))
      this.idUser = params['id']

      this.callAPI()

    })


   }



  callAPI(){



    var raw = {}

    raw['result'] = "2 ขีด"
            raw['question'] = "ใช้เป็นประจำ"
            raw['dtcheckin'] = "21/10/2020 20:15"
            raw['dtresult'] = "21/10/2020 20:20"
            raw['dtsmart'] = "5 นาที"
            raw['qrcode'] = "QR001"
  
            console.log(JSON.stringify(raw))
            this.data.push(raw)

    this.http.get('http://34.87.185.176:8787/result/'+this.idUser, {})
    .subscribe(

      datas => {

        if(datas['status'] == 200){
          var key = Object.keys(datas['message'])



          console.log(JSON.stringify(datas))
          this.count = datas['count']
          this.resultN = datas['resultN']
          this.resultP = datas['resultP']
          datas['message'].reverse();
          this.data = [];

          for(var i = 0; i < key.length;i++){
  
            var raw = {};

            if(datas['message'][i]['result_strip'] == 1){
              datas['message'][i]['result_strip'] = "2 ขีด"
            }else{
              datas['message'][i]['result_strip'] = "1 ขีด"
            }
            raw['result'] = datas['message'][i]['result_strip']
            raw['question'] = datas['message'][i]['result_question']
            raw['dtcheckin'] = datas['message'][i]['ckdt']
            raw['dtresult'] = datas['message'][i]['rdt']
            raw['dtsmart'] = datas['message'][i]['diff']
            raw['qrcode'] = datas['message'][i]['cqr']
  
            console.log(JSON.stringify(raw))
            this.data.push(raw)

            this.filteredData.push(raw)
  
  
          }

          console.log(JSON.stringify(this.data))
        }

      }
    )



  }
  ngOnInit(): void {
  }

  arrayRemove(array, id) {
    return array.filter(function (element) {
      return element.id != id;
    });
  }


  filterDatatable(event) {

    const val = event.target.value.toLowerCase();
    // get the amount of columns in the table
    const colsAmt = this.columns.length;
    // get the key names of each column in the dataset

    console.log(JSON.stringify(this.filteredData));
    const keys = Object.keys(this.filteredData[0]);
    // assign filtered matches to the active datatable
    this.data = this.filteredData.filter(function (item) {
      // iterate through each row's column data
      for (let i = 0; i < colsAmt; i++) {
        // check for a match


        if (
          item[keys[i]]
            .toString()
            .toLowerCase()
            .indexOf(val) !== -1 ||
          !val
        ) {
          // found match, return true to add to result set
          return true;
        }
      }
    });
    // whenever the filter changes, always go back to the first page
    this.table.offset = 0;
  }

  onActivate(event) {

    console.log(JSON.stringify(event.type ))

    console.log(event.row)
    if (event.type == 'click') {
      console.log(JSON.stringify(event.row));
      this.router.navigateByUrl('/detail/'+event.row.qrcode+"/"+this.idUser);
    }
  }


}
